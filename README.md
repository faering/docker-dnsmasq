# Dnsmasq

Dnsmasq docker container image that requires no specific user or root permission to function.

Docker Hub image: [https://hub.docker.com/r/faering/dnsmasq](https://hub.docker.com/r/faering/dnsmasq)

## Docker compose example

domain.conf.tmpl
```
strict-order
no-hosts
no-resolv

server=${DNSMASQ_DNS_SERVER_1}
server=${DNSMASQ_DNS_SERVER_2}

domain=${DNSMASQ_DOMAIN}
address=/${DNSMASQ_DOMAIN}/127.0.0.1
```

docker-compose.yml
```yaml
version: '3.5'
services:
    dnsmasq:
        image: faering/dnsmasq:1.0.0
        restart: always
        environment:
          DNSMASQ_DNS_SERVER_1: 1.1.1.1
          DNSMASQ_DNS_SERVER_2: 1.0.0.1
          DNSMASQ_DOMAIN: domain.test
        logging:
            options:
              max-size: 20m
        volumes:
          - ./domain.conf.tmpl:/etc/dnsmasq.d/domain.conf
        ports:
            - '53:5353/udp'
```

Configuration files can be mounted in `/etc/dnsmasq.d/`, files mounted as `.tmpl` will have their
environment variables automatically replaced.
